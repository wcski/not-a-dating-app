import { Injectable } from '@angular/core';
import * as alertify from 'alertifyjs';

@Injectable({
  providedIn: 'root'
})
export class AlertifyService {

  constructor() { }

  confirm(message: string, okCallback: () => any) {
    alertify.confirm(message, (event: any) => {
      // If there is an event (ie user clicks OK button), then we'll call our callback fn.
      // Otherwise, do nothing.
      if (event) {
        okCallback();
      } else { }
    });
  }

  // Note that we don't have any type safety with our alertify library.
  success(message: string) {
    alertify.success(message);
  }

  error(message: string) {
    alertify.error(message);
  }

  warning(message: string) {
    alertify.warning(message);
  }

  message(message: string) {
    alertify.message(message);
  }

}
