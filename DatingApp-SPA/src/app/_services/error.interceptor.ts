import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpErrorResponse, HTTP_INTERCEPTORS } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Injectable()

export class ErrorInterceptor implements HttpInterceptor {
    intercept(
        req: import('@angular/common/http').HttpRequest<any>,
        next: import('@angular/common/http').HttpHandler
    ): import('rxjs').Observable<import('@angular/common/http').HttpEvent<any>> {
        return next.handle(req).pipe(
            catchError(error => {
                if (error.status === 401) {
                    return throwError(error.statusText);
                    console.log('error status text is: ' + error.statusText);
                }
                if (error instanceof HttpErrorResponse) {
                    // These will be errors coming from the application. The
                    // paramater we're supplying to get has to match the header we're sending
                    // exactly.
                    const applicationError = error.headers.get('Application-Error');
                    if (applicationError) {
                        throwError(applicationError);
                        console.log('app error is: ' + applicationError);
                    }
                    const serverError = error.error;
                    let modelStateErrors = '';
                    // Checking type because we want to use an object method to loop over the
                    // keys of those objects and then do something with the values of those
                    // objects.
                    if (serverError.errors && typeof serverError.errors === 'object') {
                        for (const key in serverError.errors) {
                            if (serverError.errors[key]) {
                                // Build up a list of strings for each of the model state errors
                                // (password requirements, etc).
                                modelStateErrors += serverError.errors[key] + '\n';
                            }
                        }
                    }
                    return throwError(modelStateErrors || serverError || 'Server error.');

                }
            })
        );
    }
}
// We'll need to add this to our providers array.
export const ErrorInterceptorProvider = {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    // Http interceptor can have multiple interceptors.
    multi: true
};
